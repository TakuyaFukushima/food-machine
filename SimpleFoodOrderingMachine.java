import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane textPane1;

    void order (String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "What would you like to order " +food+ "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
             JOptionPane.showMessageDialog(null,
                    "Order for " +food+ " received.");
            textPane1.setText("Order for " +food+ " received");
        }
    }

    public SimpleFoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               order("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
