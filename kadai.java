import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class kadai {
    private JButton Gyudon;
    private JButton CheeseGyudon;
    private JButton Kaisendon;
    private JTextArea textArea1;
    private JButton checkOutButton;
    private JButton TonkatuTeisyoku;
    private JButton YakizakanaTeisyoku;
    private JButton SyougayakiTeisyoku;
    private JLabel money;
    private JPanel root;

    int total = 0;
    String currentText = "";
    String total_str = "";

    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai");
        frame.setContentPane(new kadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order (String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "What would you like to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            int confirmation_2 = JOptionPane.showConfirmDialog(null,
                    "Would you like a large serving of rice? (you need +¥50)",
                    "Order confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation_2 == 0) {
                total += price + 50;
            }
            else {
                total += price;
            }
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible.");
            currentText += food + "\n";
            textArea1.setText(currentText);
            total_str = String.valueOf(total);
            money.setText(total_str+"yen");
        }
    }

    public kadai() {

        Gyudon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyudon", 300);
            }
        });

        Gyudon.setIcon(new ImageIcon(
                this.getClass().getResource("gyudon.jpg")
        ));

        CheeseGyudon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cheese Gyudon", 350);
            }
        });

        CheeseGyudon.setIcon(new ImageIcon(
                this.getClass().getResource("cheese_gyudon.jpg")
        ));

        Kaisendon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kaisendon", 800);
            }
        });

        Kaisendon.setIcon(new ImageIcon(
                this.getClass().getResource("kaisendon.jpg")
        ));

        TonkatuTeisyoku.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tonkatu Teisyoku", 750);
            }
        });

        TonkatuTeisyoku.setIcon(new ImageIcon(
                this.getClass().getResource("tonkatu_teisyoku.jpg")
        ));

        YakizakanaTeisyoku.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakizakana Teisyoku", 650);
            }
        });

        YakizakanaTeisyoku.setIcon(new ImageIcon(
                this.getClass().getResource("yakizakana_teisyoku.jpg")
        ));

        SyougayakiTeisyoku.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Syougayaki Teisyoku", 600);
            }
        });

        SyougayakiTeisyoku.setIcon(new ImageIcon(
                this.getClass().getResource("syougayaki_teisyoku.jpg")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is "+total+ " yen.");
                    total = 0;
                    currentText = "";
                    total_str = "0yen";
                    textArea1.setText(currentText);
                    money.setText(total_str);
                }
            }
        });
    }
}
